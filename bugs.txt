Bug List:

-VendingMachineItem has no way of editing prices if price changes post-creation
	fix; add a setter method to VendingMachineItem.java
	**Also check if price is > 1
	
-VendingMachineItem has no way to set item name to something different post-creation
	fix; add a setter method to VendingMachineItem.java
	
-VendingMachineItem does not check if name has actual content, just that it's a string
	fix; check that length of name > 0
	
-VendingMachine removeItem method line "itemArray[slotIndex] = null;" is processed before the if(item==null) check
	this could lead to an error if the slot index was already empty.
		fix; put the "itemArray[slotIndex] = null;" after the if(item==null) error check
		
-VendingMachine insertMoney method has no upper limit on money inserted. realistically, is someone going to put 
	more than, say, $150? can only imagine this if someone was trying to convert their cash to change via .returnChange()
		fix; check amount if >0 and <150. add new error message string