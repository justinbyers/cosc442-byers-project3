/**
 * 
 */
package edu.towson.cis.cosc442.project3.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

// TODO: Auto-generated Javadoc
/**
 * The Class VendingMachineTest.
 *
 * @author Justin Byers
 */
public class VendingMachineTest {

	/** test vendingmachines vm1 and vm2. */
	VendingMachine vm1, vm2;
	
	/** test items 1 thru 4. */
	VendingMachineItem item1, item2, item3, item4;
	

	/**
	 * Sets up two dummy vending machines and
	 * populates the second machine with 4 items
	 *
	 * @throws Exception creation error
	 */
	@Before
	public void setUp() throws Exception {
		vm1 = new VendingMachine();
		vm2 = new VendingMachine();
		item1 = new VendingMachineItem("Kit Kat", 1.50);
		item2 = new VendingMachineItem("Cola", 2.00);
		item3 = new VendingMachineItem("Pretzels", 1.20);
		item4 = new VendingMachineItem("Chips", 0.99);
		
		vm2.addItem(item1, "A");
		vm2.addItem(item2, "B");
		vm2.addItem(item3, "C");
		vm2.addItem(item4, "D");
	}


	/**
	 * Tear down all dummy items and machines
	 *
	 * @throws Exception tear down exception
	 */
	@After
	public void tearDown() throws Exception {
		vm1 = null;
		vm2 = null;
		item1 = null;
		item2 = null;
		item3 = null;
		item4 = null;
	}


	/**
	 * Test get initial balance method
	 */
	@Test
	public final void testGetInitialBalance() {
		assertEquals(0.0, vm1.getBalance(), 0.001);
	}
	
	/**
	 * Test get balance method #1
	 */
	@Test
	public final void testGetBalance_one() {
		vm1.insertMoney(5.0);
		assertEquals(5.0, vm1.getBalance(), 0.001);
	}
	
	/**
	 * Test get balance method by inserting
	 * money twice, and with decimals, and 
	 * asserting the balance is accurate
	 */
	@Test
	public final void testGetBalance_two() {
		vm1.insertMoney(5.0);
		vm1.insertMoney(2.55); //check decimals are accurate too
		assertEquals(7.55, vm1.getBalance(), 0.001);
	}

	/**
	 * Test get balance method by adding item,
	 * inserting money, making purchase, and 
	 * asserting the balance is accurate
	 */
	@Test
	public final void testGetBalance_three() {
		vm1.addItem(item2, "A");
		vm1.insertMoney(5.0); //balance = $5
		vm1.makePurchase("A"); //choice A costs $2
		assertEquals(3.0, vm1.getBalance(), 0.001); //check balance = 3
	}

	/**
	 * Test add item by creating a new item, adding it,
	 * and comparing the new item to the added one
	 * in the machine
	 */
	@Test
	public final void testAddItem() {
		VendingMachineItem item = new VendingMachineItem("Kit-Kat", 1.50);
		vm1.addItem(item, "A");
		assertEquals(item, vm1.getItem("A"));
	}
	

	/**
	 * Test add free item by creating a new item
	 * with no cost (aka free), adding it to machine,
	 * and checking that it was successfully added
	 */
	@Test
	public final void testAddItem_FreeItem() {
		VendingMachineItem item = new VendingMachineItem("FREE Kit-Kat", 0);
		vm1.addItem(item, "A"); // check item with $0 cost can be added
		assertEquals(item, vm1.getItem("A"));
	}

	/**
	 * Test get item choice A
	 */
	@Test
	public final void testGetItem_A() {
		assertEquals(item1, vm2.getItem("A"));
	}

	/**
	 * Test get item choice B
	 */
	@Test
	public final void testGetItem_B() {
		assertEquals(item2, vm2.getItem("B"));
	}
	
	/**
	 * Test get item choice C
	 */
	@Test
	public final void testGetItem_C() {
		assertEquals(item3, vm2.getItem("C"));
	}

	/**
	 * Test get item choice D
	 */
	@Test
	public final void testGetItem_D() {
		assertEquals(item4, vm2.getItem("D"));
	}
	
	/**
	 * Test removing item by creating a 
	 * new item, adding it, removing it, 
	 * and checking if that slot is now empty (null)
	 */
	@Test
	public final void testRemoveItem() {
		VendingMachineItem item = new VendingMachineItem("Jolly Rancher", 0.75);
		vm1.addItem(item, "D"); 
		vm1.removeItem("D");
		assertNull(vm1.getItem("D"));
	}

	/**
	 * Test insert money by inserting money,
	 * making purchase, and checking if the
	 * balance has changed (implying the
	 * insertMoney method was successful)
	 */
	@Test
	public final void testInsertMoney() {
		vm2.insertMoney(11.50); // balance is 11.50 so we can compare getBalance post makePurchase
		vm2.makePurchase("C"); // costs 1.25
		assertEquals(10.30, vm2.getBalance(), 0.001);
	}

	/**
	 * Test make purchase method by purchasing
	 * one of each item and checking the balance
	 * (each item is priced at a certain value, so
	 * buying 1 of each will noticeably change the 
	 * end balance)
	 */
	@Test
	public final void testMakePurchase() {
		vm2.insertMoney(10.00);
		vm2.makePurchase("A"); //1.50
		vm2.makePurchase("B"); //2.00
		vm2.makePurchase("C"); //1.20
		vm2.makePurchase("D"); //0.99
		assertEquals(4.31, vm2.getBalance(), 0.001);	
	}

	/**
	 * Test make purchase when balance
	 * is not sufficient 
	 */
	@Test
	public final void testMakePurchase_InsufficientMoney() {
		vm2.insertMoney(1.00);
		assertFalse(vm2.makePurchase("A"));	//cost is 1.50, but balance is 1.00
	}
	
	/**
	 * Test make purchase when item slot is 
	 * null and empty
	 */
	@Test
	public final void testMakePurchase_InsufficientMoney_ItemNull() {
		vm1.insertMoney(1.00);
		assertFalse(vm1.makePurchase("A"));	// no item in slot A. only vm2 has items preadded
	}

	/**
	 * Test return change by checking balance
	 * post returnChange() method
	 */
	@Test
	public final void testReturnChange() {
		vm2.insertMoney(2.25);
		vm2.makePurchase("B"); // option B costs $2
		vm2.returnChange(); // changed returned: .25
		assertEquals(0.00, vm2.getBalance(), 0.001);
	}

	
	/**
	 * Test inserting negative money by
	 * checking for an error thrown
	 */
	@Test(expected = VendingMachineException.class)
	public final void testInsertMoney_LessThanZero(){
		vm2.insertMoney(-1.0); // insert money < 0 error check
	}
	
	/**
	 * Test remove item that is null by
	 * checking for an error thrown
	 */
	@Test(expected = VendingMachineException.class)
	public final void testRemoveItem_ItemNull(){
		vm1.removeItem("D"); // null item; doesnt exist; only vm2 has predded items in setUp
	}
	
	/**
	 * Test add item error when adding two items
	 * into one item slot
	 */
	@Test(expected = VendingMachineException.class)
	public final void testAddItem_ItemExistsInSlotIndex(){
		vm1.addItem(item1, "A");
		vm1.addItem(item1, "A"); // check error for duplicate item slot
	}
	
	/**
	 * Test get slot error for a slot
	 * that is not A/B/C/D
	 */
	@Test(expected = VendingMachineException.class)
	public final void testGetSlotIndex_InvalidCode(){
		vm2.addItem(item1, "E"); //invalid item slot check
	}
	
	/**
	 * Test insert money error when
	 * inserting money above the
	 * $150 balance limit
	 */
	@Test(expected = VendingMachineException.class)
	public final void testInsertMoney_AboveBalanceLimit(){
		vm2.insertMoney(149.00);
		vm2.insertMoney(1.00); //cannot go above $150
	}
}
