/**
 * 
 */
package edu.towson.cis.cosc442.project3.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The Class VendingMachineItemTest.
 *
 * @author Justin
 */
public class VendingMachineItemTest {
	
	VendingMachineItem item1, item2, item3, item4;

	/**
	 * Sets up dummy items with names and values .
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		item1 = new VendingMachineItem("Kit Kat", 1.50);
		item2 = new VendingMachineItem("Cola", 2.00);
		item3 = new VendingMachineItem("Pretzels", 1.20);
		item4 = new VendingMachineItem("Chips", 0.99);
	}

	/**
	 * Tear down all dummy items
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		item1 = null;
		item2 = null;
		item3 = null;
		item4 = null;
	}

	/**
	 * Test vending machine item creation exists
	 */
	@Test
	public final void testVendingMachineItem() {
		VendingMachineItem newItem = new VendingMachineItem("Sticky Bun", 1.50);
		assertNotNull(newItem); //confirm existence of new item
	}
	
	/**
	 * Test vending machine item name
	 */
	@Test
	public final void testVendingMachineItem_Name() {
		VendingMachineItem newItem = new VendingMachineItem("Sticky Bun", 1.50);
		assertEquals("Sticky Bun", newItem.getName()); //confirm name of new item
	}
	
	/**
	 * Test vending machine item price below zero error
	 */
	@Test(expected = VendingMachineException.class)
	public final void testVendingMachineItem_PriceBelowZero() {
		VendingMachineItem newItem = new VendingMachineItem("Negative Snack", -100.00); //check negative price item error
	}
	
	/**
	 * Test vending machine item name has no length error
	 */
	@Test(expected = VendingMachineException.class)
	public final void testVendingMachineItem_NameHasNoLength() {
		VendingMachineItem newItem = new VendingMachineItem("", 10.00); //check item with no name error
	}
	
	/**
	 * Test vending machine item get name 
	 */
	@Test
	public final void testVendingMachineItem_NameError() {
		assertNotSame("Frog", item1.getName()); //check name against some random string
	}

	/**
	 * Test get name of item A
	 */
	@Test
	public final void testGetName_A() {
		assertEquals("Kit Kat", item1.getName()); //check item A name
	}


	/**
	 * Test get name of item B
	 */
	@Test
	public final void testGetName_B() {
		assertEquals("Cola", item2.getName()); //check item b name
	}
	
	/**
	 * Test get name of item C
	 */
	@Test
	public final void testGetName_C() {
		assertEquals("Pretzels", item3.getName()); 
	}
	
	/**
	 * Test get name of item D
	 */
	@Test
	public final void testGetName_D() {
		assertEquals("Chips", item4.getName());
	}
	
	/**
	 * Test get price of item A
	 */
	@Test
	public final void testGetPrice_A() {
		assertEquals(1.50, item1.getPrice(), 0.001); //check price equals
	}
	
	/**
	 * Test get price of item B
	 */
	@Test
	public final void testGetPrice_B() {
		assertEquals(2.0, item2.getPrice(), 0.001);
	}
	
	/**
	 * Test get price of item C
	 */
	@Test
	public final void testGetPrice_C() {
		assertEquals(1.20, item3.getPrice(), 0.001);
	}
	
	/**
	 * Test get price of item D
	 */
	@Test
	public final void testGetPrice_D() {
		assertEquals(.99, item4.getPrice(), 0.001); //check price with decimals
	}
	
	/**
	 * Test set price adjustment of a 
	 * already-made item
	 */
	@Test
	public final void testSetPrice() {
		item1.setPrice(0.33);
		assertEquals(0.33, item1.getPrice(), 0.001);
	}
	
	/**
	 * Test set price price less than zero error
	 */
	@Test(expected = VendingMachineException.class)
	public final void testSetPrice_PriceLessThanZero() {
		item1.setPrice(-1.00); //check items cant be set to negative price
	}
	
	
	/**
	 * Test set name is accurate for the item
	 */
	@Test
	public final void testSetName() {
		item1.setName("Snowball");
		assertEquals("Snowball", item1.getName());
	}
	
	/**
	 * Test set name name has no length error
	 */
	@Test(expected = VendingMachineException.class)
	public final void testSetName_NameHasNoLength() {
		item1.setName(""); //check name has actual length, not empty
	}
	
}
